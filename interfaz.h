#ifndef INTERFAZ_H
#define INTERFAZ_H

#include <string>

using namespace std;

void mostrarNumDados(int vectorDados[]);
void mostrarIconDados(int vectorDados[]);
void mostrarImagenesDados(int vectorDados[]);
void turnoJugador(string nombre, int ronda, int puntaje_total, int puntaje_ronda, int cant_lanzamientos, int vectorDados[]);
void siguienteTurno(string nombre_jugadorA, string nombre_jugadorB , int ronda, int puntaje_jugadorA, int puntaje_jugadorB);
void ganador(string nombre, string apellido, int rondas);
void mostrarImagenesDados();

#endif
