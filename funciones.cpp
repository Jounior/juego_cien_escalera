#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <cctype>
//#include "interfaz.h"

using namespace std;

void mostrarNumDados(int vectorDados[])
{
    const std::string imagen_dados[6] = {"1","2","3","4","5","6"};
    for ( int i = 0; i < 6; i++ )
    {
        std::cout << imagen_dados[vectorDados[i] - 1] << " ";
    }
    cout << endl;
}

void mostrarIconDados(int vectorDados[])
{
    const std::string imagen_dados[6] = {"⚀","⚁","⚂","⚃","⚄","⚅"};
    for ( int i = 0; i < 6; i++ )
    {
        std::cout << imagen_dados[vectorDados[i] - 1] << " ";
    }
    cout << endl;
}

void turnoJugador(string nombre, int ronda, int puntaje_total, int puntaje_ronda, int cant_lanzamientos, int vectorDados[])
{
    std::cout << "---------------------------------------------------------" << std::endl;
    std::cout << " TURNO DE " << nombre << " | RONDA Nº " <<  ronda + 1 << " | PUNTAJE TOTAL: " << puntaje_total << " PUNTOS " << std::endl;
    std::cout << "---------------------------------------------------------" << std::endl;
    std::cout << " MAXIMO PUNTAJE DE LA RODA: " << puntaje_ronda << std::endl;
    std::cout << " LANZAMIENTO  Nº " << cant_lanzamientos + 1 << std::endl;
    std::cout << "---------------------------------------------------------" << std::endl;
//    mostrarIconDados(vectorDados);
    mostrarNumDados(vectorDados);
}

void siguienteTurno(std::string nombre_jugadorA, std::string nombre_jugadorB , int ronda, int puntaje_jugadorA, int puntaje_jugadorB)
{
    cout << " +-------------------------------+" << endl;
    cout << " |                               |" << endl;
    cout << " |        RONDA Nº " << ronda + 1 << "         |" << endl;
    cout << " |                               |" << endl;
    cout << " |  PUNTAJE " << nombre_jugadorA << " : " << puntaje_jugadorA <<   " PUNTOS    |" << endl;
    cout << " |  PUNTAJE " << nombre_jugadorB << " : " << puntaje_jugadorB <<   " PUNTOS  |" << endl;
    cout << " |                               |" << endl;
    cout << " +-------------------------------+" << endl;
}

void mostrarMenu(){
//    system("cls");
    cout<<"----------------------------------------------------"<<endl;
    cout<<"                     | MENU |                       "<<endl;
    cout<<"----------------------------------------------------"<<endl;
    cout<< endl;
    cout<<"       Bienvenidos al juego Escalera o Cien!        "<<endl;
    cout<< endl;
    cout<<"----------------------------------------------------"<<endl;
    cout<< endl;
    cout<<" 1 - Opcion 1 jugador "<< endl;
    cout<<" 2 - Opcion 2 jugadores "<< endl;
    cout<<" 3 - Lanzar dados "<<endl;
    cout<<" 4 - Mostrar dados "<<endl;
    cout<<" 0 - Salir del juego "<<endl;
    cout<<" Ingrese la opcion deseada: "<<endl;
}

void copyVector(int dest[], int origen[])
{
    int i;
    for ( i = 0; i < 6; i++ )
    {
        dest[i] = origen[i];
    }
}

void jugarDosJugadores() {
    string nombreJugador1, nombreJugador2;
    int puntajeTotalJugador1 = 0, puntajeTotalJugador2 = 0;
    cout << "Ingrese el nombre del jugador 1: ";
    cin >> nombreJugador1;
    cout << "Ingrese el nombre del jugador 2: ";
    cin >> nombreJugador2;
}

void lanzarDados (int vec[], int tam){
//    system("pause");

//    cout << "Lanzando dados . . . " << endl;
    int i;
    for (i=0;i<tam ;i++ ){
        vec[i]=rand()%6+1;
    }
//    system("pause");
}

void cargarDadosManual(int vec[],int tam ){
    int i;
    for (i=0;i<tam ;i++ ){
        cout<<"Ingrese el valor de dado "<<i+1<<": ";
        cin>>vec[i];
    }
    cout<<"Dados correctamente cargados..."<<endl;
}
void ponerDadosEn0(int vec[],int tam ){
    int i;
    for (i=0;i<tam ;i++ ){
        vec[i]=0;
    }
    cout<<"Dados seteados en 0..."<<endl;
}

void mostrarDados (int vec[],int tam ){
    int i;
    for (i=0;i<tam ;i++ ){
        cout<<"Dado "<<i+1<<": "<<vec[i]<<endl;
    }
//    system("pause");
}

void limpiarPantalla() {
//    system("cls");
}




string ingresarNombre()
{
    string nombre;
    cout << "Ingrese el nombre del jugador "; cin >> nombre;
    return nombre;
}

bool dadosEscalera(int vec[])
{
    int i, j, valor_dado, n_veces = 0;
    for ( i = 0; i < 3; i++ )
    {
        valor_dado = i + 1;
        for ( j = 0; j < 3; j++ )
        {
            if (valor_dado == vec[j])
            {
                n_veces += 1;
            }
        }
    }

    if ( n_veces == 6 )
        return true;
    return false;
}

bool dadosIguales(int vec[])
{
    int i, dado_valor, n_veces = 0;
    dado_valor = vec[0];

    for ( i = 1; i < 3; i++ )
    {
        if ( dado_valor == vec[i] )
        {
            n_veces += 1;
        }
    }

    if ( n_veces == 5 )
        return true;
    return false;
}

int sumarVector(int vec[])
{
    int i, suma = 0;
    for ( i = 0; i < 6; i++ )
    {
        suma += vec[i];
    }
    return suma;
}

int revisarCombinación(int vec[])
{
    int valor_dado;
    if ( dadosEscalera(vec) )
    {
        return 1;
//        return GANA_PARTIDA;  // 1 - Ganamos
    }
    else if ( dadosIguales(vec) )
    {
        valor_dado = vec[0];
        if ( valor_dado == 6 )
             return 0;
//            return RESETEA_PUNTAJE;    // 0 - resetea el valor
        return valor_dado * 10;
    }
    else
    {
        return sumarVector(vec);
    }
}

int lanzamiento(int vectorDados[])
{
    int puntaje;

    lanzarDados(vectorDados, 6);
    puntaje = revisarCombinación(vectorDados);
    return puntaje;
}

void jugarUnJugador()
{
    string jugador_nombre;
    string pc_nombre = "PC";
    jugador_nombre = ingresarNombre();

    int rondas = 0;
    int puntaje_ronda = 0;
    int puntaje_lanzamiento = 0;
    int mejor_lanzamiento = 0;
    int max_lanzamientos = 3;
    int jugador_puntajeTotal = 0;
    int pc_puntajeTotal = 0;
    int vec[6];
    int mejor_vec[6];

    while ( jugador_puntajeTotal <= 100 && pc_puntajeTotal <= 100 && rondas <= 10 )
    {
        // Lanzamiento jugador
        for ( int num_lanzamiento = 0; num_lanzamiento < max_lanzamientos; num_lanzamiento++ )
        {
            puntaje_lanzamiento = lanzamiento(vec);
            if ( puntaje_lanzamiento == 1 )
            {
                copyVector(mejor_vec, vec);
                mejor_lanzamiento = num_lanzamiento;
                cout << " ESCALERA! GANASTE LA PARTIDA! " << endl;
                break;
            }
            else if ( puntaje_lanzamiento == 0 )
            {
                copyVector(mejor_vec, vec);
                mejor_lanzamiento = num_lanzamiento;
                jugador_puntajeTotal = 0;
            }

            if ( puntaje_ronda < puntaje_lanzamiento )
            {
                copyVector(mejor_vec, vec);
                mejor_lanzamiento = num_lanzamiento;
                puntaje_ronda = puntaje_lanzamiento;
            }
        }
        jugador_puntajeTotal += puntaje_ronda;
        turnoJugador(jugador_nombre, rondas, jugador_puntajeTotal,  puntaje_ronda, mejor_lanzamiento, mejor_vec);
        siguienteTurno(jugador_nombre, pc_nombre, rondas, jugador_puntajeTotal, pc_puntajeTotal);

        // Reiniciamos valores
        puntaje_ronda = 0;
        mejor_lanzamiento = 0;
        // Lanzamiento PC
        for ( int num_lanzamiento = 0; num_lanzamiento < max_lanzamientos; num_lanzamiento++ )
        {
            puntaje_lanzamiento = lanzamiento(vec);
            if ( puntaje_lanzamiento == 1 )
            {
                mejor_lanzamiento = num_lanzamiento;
                cout << " ESCALERA! GANASTE LA PARTIDA! " << endl;
                break;
            }
            else if ( puntaje_lanzamiento == 0 )
            {
                mejor_lanzamiento = num_lanzamiento;
                jugador_puntajeTotal = 0;
            }

            if ( puntaje_ronda < puntaje_lanzamiento )
            {
                mejor_lanzamiento = num_lanzamiento;
                puntaje_ronda = puntaje_lanzamiento;
            }
        }
        pc_puntajeTotal += puntaje_ronda;
        turnoJugador(pc_nombre, rondas, pc_puntajeTotal,  puntaje_ronda, mejor_lanzamiento, mejor_vec);
        siguienteTurno(jugador_nombre, pc_nombre, rondas, jugador_puntajeTotal, pc_puntajeTotal);

        rondas += 1;
    }

    if ( jugador_puntajeTotal >= 100 && pc_puntajeTotal >= 100 )
    {
        cout << " EMPATE " << endl;
    }
    else if ( jugador_puntajeTotal >= 100 )
    {
        cout << "EL GANADOR ES: " << jugador_nombre << endl;
    }
    else
    {
        cout << "EL GANADOR ES: " << pc_nombre << endl;
    }
}
