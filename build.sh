#!/usr/bin/bash

set -xe

CC=g++
CFLAGS="-Wall -Wextra"

$CC $FLAGS -o main main.cpp funciones.cpp
