#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED

#include <string>

using namespace std;

void jugarUnJugador();
void jugarDosJugadores();
void cargarDadosManual(int vec[], int tam);
void lanzarDados(int vec[], int tam);
void mostrarDados(int vec[], int tam);
void ponerDadosEn0(int vec[], int tam);
void limpiarPantalla();
void mostrarMenu();


string ingresarNombre();
bool dadosEscalera(int vec[]);
bool dadosIguales(int vec[]);
int sumarVector(int vec[]);
int revisarCombinación(int vec[]);
int lanzamiento();
int iniciarLanzamientos();




#endif // FUNCIONES_H_INCLUDED
