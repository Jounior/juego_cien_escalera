#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string>

using namespace std;
#include "funciones.h"

int main()
{
    srand(time(NULL));
    const int NUM_DADOS = 6;
    const int PUNTOS_PARA_GANAR = 100;
    const int MAX_RONDAS = 10;
    int opcion=1;
    int dados[NUM_DADOS]= {1,2,3,4,5,6};

    while(opcion!=0)
    {
        mostrarMenu();
        cin >> opcion;

        switch (opcion)
        {

        case 1:
            jugarUnJugador();
            break;
        case 2:
            jugarDosJugadores();
            break;
        case 3:
            lanzarDados (dados, NUM_DADOS);
            break;
        case 4:
            mostrarDados(dados, NUM_DADOS);
            break;
        case 143:
            cargarDadosManual(dados, NUM_DADOS);
            break;
        case 0:
            cout<<"Gracias por jugar! Saliendo del juego..."<<endl;
            break;
        default:
            cout<<"Ingrese una opcion correcta."<<endl;
//            system("pause");
            break;
        }
    }
    return 0;
}
